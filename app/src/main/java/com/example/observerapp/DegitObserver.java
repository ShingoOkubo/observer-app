package com.example.observerapp;

import android.widget.TextView;

public class DegitObserver implements Observer {

    private Boolean mFlag = false;

    public void update(NumberGenerator generator, TextView view) {
        // 観察対象から通知を受けたら処理する。（今回はログ出力）
        System.out.println("DegitObserver:" + generator.getNumber());

        if (!mFlag)
            view.setText("★Log\r\n");

        if (view.getText() != null && !view.getText().equals("")) {
            mFlag = true;
            String textViewText = view.getText().toString();
            textViewText += "DegitObserver:" + generator.getNumber() + "\r\n";
            String logText = textViewText;
            view.setText(logText);
        }

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
