package com.example.observerapp;

import android.os.AsyncTask;
import android.util.Log;

import com.example.observerapp.listener.AsyncTaskCallbacks;

public class SampleAsyncTask extends AsyncTask<Void, Long, Long> {

    /**
     * コールバック
     */
    private AsyncTaskCallbacks callback = null;

    private Long mSec;

    /**
     * コンストラクタ（コールバックを登録）
     * @param callback
     */
    public SampleAsyncTask(AsyncTaskCallbacks callback) {
        this.callback = callback;
    }

    /**
     * バックグラウンド処理の前に実行される処理
     */
    @Override
    protected void onPreExecute() {
        Log.v("AsyncTask", "onPreExecute");
        mSec = 0L;
    }

    /**
     * バックグラウンド処理
     * @param voids
     * @return
     */
    @Override
    protected Long doInBackground(Void... voids) {
        Log.v("AsyncTask", "doInBackground");
        try {
            for (int i=0; i<10; i++) {
                Thread.sleep(1000);
                mSec++;
                Log.v("AsyncTask", mSec + "sec");
                if (isCancelled()) {
                    Log.v("AsyncTask", "CANCEL");
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return mSec;
    }

    /**
     * AsyncTask終了時にコールされる処理
     * @param aLong
     */
    @Override
    protected void onPostExecute(Long aLong) {
        //super.onPostExecute(aLong);
        Log.v("AsyncTask", "onPostExecute");
        callback.onTaskFinished();
    }

    /**
     * AsyncTaskキャンセル時にコールされる処理
     */
    @Override
    protected void onCancelled() {
        //super.onCancelled();
        Log.v("AsyncTask", "onCancelled");
        callback.onTaskCancelled();
    }
}
