package com.example.observerapp.listener;

public interface AsyncTaskCallbacks {
    // 終了
    public void onTaskFinished();
    // キャンセル
    public void onTaskCancelled();
}
