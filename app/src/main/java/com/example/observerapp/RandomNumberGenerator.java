package com.example.observerapp;

import android.widget.TextView;

import java.util.Random;

public class RandomNumberGenerator extends NumberGenerator {

    private final Random random = new Random();

    private int number;

    public int getNumber() {
        return number;
    }

    public void execute(TextView view) {
        for (int i = 0; i < 20; i++) {
            number = random.nextInt(50);
            // 乱数を生成する都度、Observerへ通知する
            notifyObservers(view);
        }
    }
}
