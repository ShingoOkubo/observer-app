package com.example.observerapp;

import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class NumberGenerator {

    private final ArrayList observers = new ArrayList();

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void deleteObserver(Observer observer) {
        observers.remove(observer);
    }

    protected void notifyObservers(TextView view) {
        Iterator iterator = observers.iterator();
        while (iterator.hasNext()) {
            Observer obserber = (Observer)iterator.next();
            // 乱数を生成する都度、Observerへ通知する
            obserber.update(this, view);
        }
    }

    public abstract int getNumber();

    public abstract void execute(TextView view);
}
