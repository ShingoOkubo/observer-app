package com.example.observerapp;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ThirdActivity extends AppCompatActivity {

    TextView showLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        showLog = findViewById(R.id.show_log);

        Button mButton = findViewById(R.id.execute_observer);
        mButton.setOnClickListener(v -> executeObserver());
    }

    private void executeObserver() {
        // 具体的な’観察対象’の生成
        NumberGenerator generator = new RandomNumberGenerator();
        // 具体的な’観察者’の生成
        Observer degitObserver = new DegitObserver();
        // 具体的な’観察対象’に具体的な’観察者’を登録
        generator.addObserver(degitObserver);
        // 状態の更新
        generator.execute(showLog);
    }
}
