package com.example.observerapp;

import android.widget.TextView;

public interface Observer {
    void update(NumberGenerator generator, TextView view);
}
