package com.example.observerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.observerapp.listener.AsyncTaskCallbacks;

public class SubActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskCallbacks {

    private Button mBtn;

    private boolean mFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        mBtn = findViewById(R.id.btn_id);
        mBtn.setText("Start");
        mBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // SampleAsyncTaskのインスタンスを生成する.
        SampleAsyncTask task = new SampleAsyncTask(this);
        // 非同期処理開始
        if (mFlag == false) {
            task.execute();
            mBtn.setText("Cancel");
            mFlag = true;
        } else {
            task.cancel(false);
        }
    }

    /**
     * AsyncTaskCallbacks#onTaskFinished実装
     */
    @Override
    public void onTaskFinished() {
        Toast.makeText(this, "AsyncTaskが終了しました", Toast.LENGTH_LONG).show();
        mBtn.setText("Start");
        mFlag = false;
    }

    /**
     * AsyncTaskCallbacks#onTaskCancelled実装
     */
    @Override
    public void onTaskCancelled() {
        Toast.makeText(this, "AsyncTaskがキャンセルされました", Toast.LENGTH_LONG).show();
        mBtn.setText("Start");
        mFlag = false;
    }
}
